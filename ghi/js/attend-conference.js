window.addEventListener('DOMContentLoaded', async () => {

    const selectTag = document.getElementById(`conference`);
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement(`option`);
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    selectTag.classList.remove(`d-none`)
    const loadTag = document.getElementById(`loading-conference-spinner`);
    loadTag.classList.add(`d-none`)

    const formTag = document.getElementById(`create-attendee-form`)
    
    formTag.addEventListener(`submit`, async event => {
        event.preventDefault(); 
        
        let formData = new FormData(formTag);
        let dataObject = Object.fromEntries(formData)
        let json = JSON.stringify(dataObject);
      
        console.log(json)
        const attendeeURL = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post", 
            body: json, 
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(attendeeURL, fetchConfig);
        
        if (response.ok){
            const successTag = document.getElementById(`success-message`)
            successTag.classList.remove('d-none')
            // const attendeeTag = document.getElementById(`create-attendee-form`)
            formTag.classList.add(`d-none`)
            console.log("works")
        
            
            formTag.reset(); 
            const newAttendee = await response.json();
            
           
        }


    })
    
    }
  
  });