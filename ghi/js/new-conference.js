window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/locations/"; 
    
    
    // try {
    //     const response = await fetch(url);
    //     console.log("test")
    //     if(!response.ok){
    //         // do something
    //     } else {

    //         const data = await response.json();
    //         const selectTag = document.getElementById('location')
    //         // console.log(data)

    //         for (let location of data.locations){
              
    //             console.log(location)
                
    //             const option = document.createElement(`option`)
    //             option.value = location.id
    //             option.innerHTML = location.name
    //             selectTag.appendChild(option)
                
                
    //         }
    //     }

        // const formTag = document.getElementById('create-conference-form')
       

        formTag.addEventListener('submit', async event => {
            console.log("inside even listener")
            event.preventDefault();
    
            let formData = new FormData(formTag); 
            let dataObject = Object.fromEntries(formData)
          
            let json = JSON.stringify(dataObject);
         
    
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
           
            const fetchConfig = {
                method:"post",
                body: json, 
                headers: {
                    'Content-Type': 'application/json', 
        
                }, 
            }; 
    
            
        
            const response = await fetch(conferenceUrl, fetchConfig);
            console.log("after response loads")
            
            if (response.ok){
                console.log("works")
                formTag.reset(); 
                const newConference = await response.json();
               
            }
        })

    } catch(e){

    }

    });