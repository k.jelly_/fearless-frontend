// class LocationForm extends React.Component {
//     componentDidMount() {
//       const url = 'http://localhost:8000/api/states/';
  
//       const response = await fetch(url);
  
//       if (response.ok) {
//         const data = await response.json();
  
//         const selectTag = document.getElementById('state');
//         for (let state of data.states) {
//           const option = document.createElement('option');
//           option.value = state.abbreviation;
//           option.innerHTML = state.name;
//           selectTag.appendChild(option);
//         }
//       }
//     }
  
//     // render method...
  
//   }

  
    // render method...
  
    // const formTag = document.getElementById('create-location-form')

    // formTag.addEventListener('submit', async event => {
    //     event.preventDefault();


    //     let formData = new FormData(formTag); 
    //     let dataObject = Object.fromEntries(formData)
      

    //     let json = JSON.stringify(dataObject);
     

    //     const locationURL = 'http://localhost:8000/api/locations/';
       
    //     const fetchConfig = {
    //         method:"post",
    //         body: json, 
    //         headers: {
    //             'Content-Type': 'application/json', 
    
    //         }, 
    //     }; 

        
    
        // const response = await fetch(locationURL, fetchConfig);
        
        // if (response.ok){
        //     console.log("works")
        //     formTag.reset(); 
        //     const newLocation = await response.json();
           
        // }
  
// }




window.addEventListener('DOMContentLoaded', async () => {

   class LocationForm extends React.Component{
    componentDidMount() {
        const locationUrl = "http://localhost:8000/api/states/"; 
    
        const response = await fetch(locationUrl);
            if (response.ok) {
                const data = await response.json() 
         
                const selectTag = document.getElementById(`state`)
           
                console.log(data)
               
    
                for (let state of data.states){
             
                    console.log(state)
                    const option = document.createElement('option')
                 
                    option.value = state.abbreviation
                    option.innerHTML = state.name
                    selectTag.appendChild(option)
                    
                }
            }
    
    }
   }

    const formTag = document.getElementById('create-location-form')

    formTag.addEventListener('submit', async event => {
        event.preventDefault();


        let formData = new FormData(formTag); 
        let dataObject = Object.fromEntries(formData)
      

        let json = JSON.stringify(dataObject);
     

        const locationURL = 'http://localhost:8000/api/locations/';
       
        const fetchConfig = {
            method:"post",
            body: json, 
            headers: {
                'Content-Type': 'application/json', 
    
            }, 
        }; 

        
    
        const response = await fetch(locationURL, fetchConfig);
        
        if (response.ok){
            console.log("works")
            formTag.reset(); 
            const newLocation = await response.json();
           
        }
    })


     
});



