import React from "react"

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            presentations: 0,
            attendees: 0,
            locations: []

        }
        this.state = { locations: [] };
        this.handleStateChange = this.handleStateChange.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleAttendeesChange = this.handleAttendeesChange.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleEndChange = this.handleEndChange.bind(this)
        this.handleStartChange = this.handleStartChange.bind(this)
        this.handlePresentationChange = this.handlePresentationChange.bind(this)
        this.handleSubmitChange = this.handleSubmitChange.bind(this)


    }


    handleStateChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })

    }

    handleLocationChange(event) {
        const value = event.target.value
        this.setState({ location: value })
    }

    handleStartChange(event) {
        const value = event.target.value
        this.setState({ starts: value })
    }

    handleEndChange(event) {
        const value = event.target.value
        this.setState({ ends: value })
    }

    handleDescriptionChange(event) {
        const value = event.target.value
        this.setState({ description: value })

    }

    handlePresentationChange(event) {
        const value = event.target.value
        this.setState({ presentation: value })
    }

    handleAttendeesChange(event) {
        const value = event.target.value
        this.setState({ attendees: value })
    }

    async handleSubmitChange(event) {

        event.preventDefault();
        const data = { ...this.location };
        const id = data.location.id
        data.href = `http://localhost:8000/api/conferences/${id}/`
        data.max_attendees = data.attendees
        data.max_presentations = data.presentations
        delete data.attendees
        delete data.presentations
        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)

            // const cleared = {
            //     name: '', 
            //     starts: '',
            //     ends: '',
            //     description: '', 
            //     presentations: '', 
            //     attendees: '', 
            //     location: '',

            // }

            // this.setState(cleared)


        }
    }

    async componentDidMount() {

        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }



    render() {
        return (


            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-5">
                        <h1>Create a new Conference</h1>
                        <form onChange={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>

                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStartChange} placeholder="Start date" required type="date" name="starts" id="starts"
                                    className="form-control" />
                                <label htmlFor="room_count">Start date</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={this.handleEndChange} placeholder="End date" required type="date" name="ends" id="ends"
                                    className="form-control" />
                                <label htmlFor="room_count">End date</label>
                            </div>

                            <div className="mb-3">
                                <label onChange={this.handleDescriptionChange} htmlFor="description" className="col-sm-2 col-form-label">Description</label>
                                <div className="col-sm-10">


                                    <textarea className="form-control" name="description" id="description" rows="3"
                                        styles="width:400px;"></textarea>
                                </div>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={this.handlePresentationChange} placeholder="Max presentations" required type="number" name="max_presentations"
                                    id="Maximum presentations" className="form-control" />
                                <label htmlFor="name">Maximum presentations</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={this.handleAttendeesChange} placeholder="Name" required type="number" name="max_attendees"
                                    id="Maximum attendees" className="form-control" />
                                <label htmlFor="name">Maximum attendees</label>
                            </div>

                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required id="location" name="location"
                                    className="form-select" value={this.state.location}>
                                    <option value="">Choose a location</option>

                                    {this.state.locations.map(location => {

                                        return (
                                            <option key={location.name} value={location.name}>
                                                {location.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </ form>
                    </div>
                </div>
            </div>

        );
    }

}

export default ConferenceForm